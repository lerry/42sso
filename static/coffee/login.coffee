fancybox = $.fancybox

host = ->
    href = CONST.HOST 
    l = location
    if l.port
        href += (":"+l.port)
    return href

reg = (data) ->
    fancybox(
        afterShow : ->
            SsoNew = $('#SsoNew').submit(
                ->
                    error = {}
                    data.name = name_val = $.trim name.val()
                    if not name_val
                        error.name = "请填写名号" 
                    data.sex = sex_val = sex.val() - 0
                    if not sex_val 
                        error.sex = "请选择性别"
                    
                    if not errtip.set(error)
                        fancybox.showLoading()
                        $.getJSON(
                            "#{SSO}auth/reg/mail?callback=?"
                            data
                            ->
                                mail = $.escape data.mail
                                fancybox({
                                    content : """<div id="SsoSended"><p>验证邮件已经发送到</p><p>#{mail}</p><p>注意 : 可能被误判为垃圾邮件</p><a class="xlarge uibutton" href="http://#{mail.split('@')[1]}" target="_blank">点此查收</a></div>"""
                                })
                                fancybox.hideLoading()
                        )
                    return false
            )
            SsoNew.find('.uibutton').click(->SsoNew.submit())
            errtip = $.errtip SsoNew
            name = SsoNew.find("input[name=name]").focus()
            sex = SsoNew.find("select[name=sex]")
        
        content : """<form id="SsoNew"><div class="submitHide" style="position:absolute"><input type="submit"></div><div><label>名号</label><input name="name"><span class="tip"/></div><div><label>性别</label><select name="sex"><option value="0">-</option><option value="1">男</option><option value="2">女</option></select><span class="tip"/></div><a href="javascript:void(0)" class="uibutton xlarge">创建帐号</a></form>"""
    )

HOST = host()
SSO = "https://42sso.sinaapp.com/"
$.SSO =  
    logout : ->
        fancybox.showLoading()
        if $("#SsoLogout").length == 0
            $('body').append("""<iframe id="SsoLogout" style="display:none" src="/auth/logout?_xsrf=#{$.cookie.get('_xsrf')}">""")

    login : ->
        _ = $.html()
        _ """<div id="SsoLogin"><div id="SsoOauth"><h2>或者，使用合作网站帐号登录</h2>"""
        for [en, cn] in [
            ["weibo", "新浪微博"],
            ["douban", "Douban"],
            ["google", "Google"],
        ]
            _ """<a href="javascript:void(0)" class="#{en}"><span class="uibutton xlarge">#{cn}</span></a>"""

        _ """</div><form id="SsoReg"><h2>注册 或 登录</h2><div><input name="mail" type="email" placeholder="邮箱"><span class="tip"/></div><div><input name="password" type="password" placeholder="密码"><span class="tip"/></div><div id="SsoRegFoot"><span class="forever"><input type="checkbox" id="SsoRegForver"><label for="SsoRegForver">下次自动登录</label></span><a class="uibutton xlarge" href="javascript:void(0)">启航</a><div class="submitHide"><input type="submit"></div></form></div></div>"""
        fancybox
            content : _.html() 
            scrolling : "no"
            autoCenter : true
            afterShow:->
                $('#SsoReg input[name=mail]').focus()
            beforeShow:->
                SsoReg = $("#SsoReg").submit(->
                    mail = SsoReg.find('input[name=mail]').val()
                    password = SsoReg.find('input[type=password]').val()
                    forever = SsoReg.find('#SsoRegForver').attr('check')
                    data = {
                        mail : mail
                        host : HOST
                        password : password
                    }
                    if $("#SsoRegForver")[0].checked
                        data.expire = 999
                    error = {}
                    if not mail
                        error.mail = "请输入邮箱"
                    if password.length < 6
                        error.password = "密码最少为六位"
                    if not password
                        error.password = "请输入密码"
                    if not errtip.set(error)
                        fancybox.showLoading()
                        $.getJSON(
                            "#{SSO}auth/login/mail?callback=?"
                            data
                            (o) ->
                                if o.error
                                    fancybox.hideLoading()
                                    errtip.set o.error
                                else if o.new
                                    fancybox.hideLoading()
                                    reg(data) 
                                else
                                    $('body').append("""<iframe
style="display:none"
src="#{SSO}auth/logined?expire=#{data.expire or 0}"/>"""
                                    )
                        )
                    return false 
                ) 
                SsoReg.find('.uibutton').click(->SsoReg.submit())
                errtip = $.errtip SsoReg

                $(".fancybox-skin").css({padding:0})
                SsoOauth = $("#SsoOauth")
                SsoOauth.find('a').click(->
                    className = @className
                    link = "#{SSO}oauth/#{className}?host=#{HOST}"
                    if className == "google" 
                        window.open(link, className, "location=0,status=0,resizable=no,width=500,height=500,menubar=no,toolbar=no")
                        return
                    $("#SsoLogin").replaceWith  """<div id="SsoIframeW"><iframe src="#{link}" scrolling="no"></iframe></div>"""
                    fancybox.reposition()
                    fancybox.update()
                )
