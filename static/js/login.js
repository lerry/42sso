(function() {
  var HOST, SSO, fancybox, host, reg;

  fancybox = $.fancybox;

  host = function() {
    var href, l;
    href = CONST.HOST;
    l = location;
    if (l.port) {
      href += ":" + l.port;
    }
    return href;
  };

  reg = function(data) {
    return fancybox({
      afterShow: function() {
        var SsoNew, errtip, name, sex;
        SsoNew = $('#SsoNew').submit(function() {
          var error, name_val, sex_val;
          error = {};
          data.name = name_val = $.trim(name.val());
          if (!name_val) {
            error.name = "请填写名号";
          }
          data.sex = sex_val = sex.val() - 0;
          if (!sex_val) {
            error.sex = "请选择性别";
          }
          if (!errtip.set(error)) {
            fancybox.showLoading();
            $.getJSON("" + SSO + "auth/reg/mail?callback=?", data, function() {
              var mail;
              mail = $.escape(data.mail);
              fancybox({
                content: "<div id=\"SsoSended\"><p>验证邮件已经发送到</p><p>" + mail + "</p><p>注意 : 可能被误判为垃圾邮件</p><a class=\"xlarge uibutton\" href=\"http://" + (mail.split('@')[1]) + "\" target=\"_blank\">点此查收</a></div>"
              });
              return fancybox.hideLoading();
            });
          }
          return false;
        });
        SsoNew.find('.uibutton').click(function() {
          return SsoNew.submit();
        });
        errtip = $.errtip(SsoNew);
        name = SsoNew.find("input[name=name]").focus();
        return sex = SsoNew.find("select[name=sex]");
      },
      content: "<form id=\"SsoNew\"><div class=\"submitHide\" style=\"position:absolute\"><input type=\"submit\"></div><div><label>名号</label><input name=\"name\"><span class=\"tip\"/></div><div><label>性别</label><select name=\"sex\"><option value=\"0\">-</option><option value=\"1\">男</option><option value=\"2\">女</option></select><span class=\"tip\"/></div><a href=\"javascript:void(0)\" class=\"uibutton xlarge\">创建帐号</a></form>"
    });
  };

  HOST = host();

  SSO = "https://42sso.sinaapp.com/";

  $.SSO = {
    logout: function() {
      fancybox.showLoading();
      if ($("#SsoLogout").length === 0) {
        return $('body').append("<iframe id=\"SsoLogout\" style=\"display:none\" src=\"/auth/logout?_xsrf=" + ($.cookie.get('_xsrf')) + "\">");
      }
    },
    login: function() {
      var cn, en, _, _i, _len, _ref, _ref1;
      _ = $.html();
      _("<div id=\"SsoLogin\"><div id=\"SsoOauth\"><h2>或者，使用合作网站帐号登录</h2>");
      _ref = [["weibo", "新浪微博"], ["douban", "Douban"], ["google", "Google"]];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        _ref1 = _ref[_i], en = _ref1[0], cn = _ref1[1];
        _("<a href=\"javascript:void(0)\" class=\"" + en + "\"><span class=\"uibutton xlarge\">" + cn + "</span></a>");
      }
      _("</div><form id=\"SsoReg\"><h2>注册 或 登录</h2><div><input name=\"mail\" type=\"email\" placeholder=\"邮箱\"><span class=\"tip\"/></div><div><input name=\"password\" type=\"password\" placeholder=\"密码\"><span class=\"tip\"/></div><div id=\"SsoRegFoot\"><span class=\"forever\"><input type=\"checkbox\" id=\"SsoRegForver\"><label for=\"SsoRegForver\">下次自动登录</label></span><a class=\"uibutton xlarge\" href=\"javascript:void(0)\">启航</a><div class=\"submitHide\"><input type=\"submit\"></div></form></div></div>");
      return fancybox({
        content: _.html(),
        scrolling: "no",
        autoCenter: true,
        afterShow: function() {
          return $('#SsoReg input[name=mail]').focus();
        },
        beforeShow: function() {
          var SsoOauth, SsoReg, errtip;
          SsoReg = $("#SsoReg").submit(function() {
            var data, error, forever, mail, password;
            mail = SsoReg.find('input[name=mail]').val();
            password = SsoReg.find('input[type=password]').val();
            forever = SsoReg.find('#SsoRegForver').attr('check');
            data = {
              mail: mail,
              host: HOST,
              password: password
            };
            if ($("#SsoRegForver")[0].checked) {
              data.expire = 999;
            }
            error = {};
            if (!mail) {
              error.mail = "请输入邮箱";
            }
            if (password.length < 6) {
              error.password = "密码最少为六位";
            }
            if (!password) {
              error.password = "请输入密码";
            }
            if (!errtip.set(error)) {
              fancybox.showLoading();
              $.getJSON("" + SSO + "auth/login/mail?callback=?", data, function(o) {
                if (o.error) {
                  fancybox.hideLoading();
                  return errtip.set(o.error);
                } else if (o["new"]) {
                  fancybox.hideLoading();
                  return reg(data);
                } else {
                  return $('body').append("<iframe\nstyle=\"display:none\"\nsrc=\"" + SSO + "auth/logined?expire=" + (data.expire || 0) + "\"/>");
                }
              });
            }
            return false;
          });
          SsoReg.find('.uibutton').click(function() {
            return SsoReg.submit();
          });
          errtip = $.errtip(SsoReg);
          $(".fancybox-skin").css({
            padding: 0
          });
          SsoOauth = $("#SsoOauth");
          return SsoOauth.find('a').click(function() {
            var className, link;
            className = this.className;
            link = "" + SSO + "oauth/" + className + "?host=" + HOST;
            if (className === "google") {
              window.open(link, className, "location=0,status=0,resizable=no,width=500,height=500,menubar=no,toolbar=no");
              return;
            }
            $("#SsoLogin").replaceWith("<div id=\"SsoIframeW\"><iframe src=\"" + link + "\" scrolling=\"no\"></iframe></div>");
            fancybox.reposition();
            return fancybox.update();
          });
        }
      });
    }
  };

}).call(this);
