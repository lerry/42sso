#!/usr/bin/env python
#coding:utf-8

import _envi
from view._base import View as _View
from config import SSO_SECRET, HOST
from json import loads
from time import time
from base64 import urlsafe_b64encode
from hashlib import sha256
from urllib import urlencode



def sign(secret, data):
    if not secret:
        return None
    return urlsafe_b64encode(sha256(data+secret).digest()).rstrip('=')


class SignView(_View):
    def prepare(self):
        super(SignView, self).prepare()
        o = self.get_argument('o')
        if self.get_argument('sign') != sign(SSO_SECRET, o):
            self.send_error(403, message='sign not match')
        else:
            self.o = o = loads(o)
            _time = o[0]
            if abs(_time - time()) > 600:
                self.send_error(403, message='time not match')


class P3PView(_View):
    def set_cookie(self, *args, **kwds):
        self.add_header('P3P', 'CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"')
        super(P3PView, self).set_cookie(*args, **kwds)

