#!/usr/bin/env python
# coding:utf-8
import _env
from zapp._plugin.base.view import View as _View
from jsdict import JsDict
from zapp._plugin.auth.model.user_session import UserSession
from zapp._plugin.auth.model.user import User
from misc.config import HOST

class UserDict(JsDict):
    def __nonzero__(self):
        return self.id

class View(_View):
    def get_current_user(self):
        current_user_id = self.current_user_id
        if current_user_id:
            return User.get(current_user_id)
        o = UserDict()
        o.id = 0
        o.name = ''
        o.version = 0
        o.icon_id = 0
        return o

    @property
    def current_user_id(self):
        if not hasattr(self, '_current_user_id'):
            s = self.get_cookie('S')
            self._current_user_id = _current_user_id = UserSession.id_by_b64_secret(s)
            if s and not _current_user_id:
                host = self.request.host
                self.clear_cookie('S', domain=HOST )
        return self._current_user_id

    @current_user_id.setter
    def current_user_id(self, value):
        self._current_user_id = value

