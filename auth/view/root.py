#!/usr/bin/env python
#coding:utf-8
from _route import route
import logging
from view._base import View
from _base import sign
# from zapp._plugin.auth.model.user_session import UserSession
# from zapp._plugin.auth.model.user import User
# from zapp._plugin.auth.model.mail import Mail 
from json import loads
from config import SSO_SECRET, HOST
from time import time
from urllib import quote
from json import dumps

SSO_URL = 'https://42sso.sinaapp.com'
# 登录页面是一个 iframe
# http://auth.42qu.com/api/v1/login
# 如果已经登录 
#     返回 校验签名 , user_id , 用户名称 
# 
# http://auth.42qu.com/cookie/set -> redirect http://42btc.com/xxx/xxx

# http://t.sina.com.cn/ajaxlogin.php?framelogin=0&callback=?&retcode=0

# 每次用户登录 , 如果发现他没有邮箱 , 都首先跳转到该页面 
# 如果当前用户没有绑定邮箱
# 1. 当前邮箱没有绑定用户  -> 绑定邮箱到此用户
# 2. 如果当前邮箱已经绑定用户 -> 记录邮箱到用户的关联 , 但是不激活
# 如果当前用户已经绑定邮箱 , 但是邮箱不相同 -> 记录到这个用户的备用邮箱(只记录一个)

# 如果没有用户名 , 用这个获取用户名
# http://portalcgi.music.qq.com/fcgi-bin/music_mini_portal/cgi_getuserinfo.fcg 


#一、 google.com 和 youtube.com
#情况一：都未登录，但是浏览过 youtube
#登录 google.com 通过 https://accounts.google.com/ServiceLoginAuth 验证用户名密码
#通过验证后跳转到 https://accounts.google.com/CheckCookie 检查是否有浏览过其它应用，比如说 youtube，这时就再次跳转到 https://accounts.youtube.com/accounts/SetSID 进行登录
#最后再跳回 google.com 的首页

from _base import SignView, P3PView

class View(SignView, P3PView):
    def finish(self, chunk=None):
        if self._finished:
            return
        if chunk is None:
            if self.get_argument('reload', 0):
                return self.reload()
            else:
                chunk = '1'
        super(View, self).finish(chunk)


@route('/auth/login')
class _(View):
    def get(self):
        o = self.o
        time, user_id, version, name, icon_id, mail, expire = self.o
        #print self.o
        print time, user_id, version, name, icon_id, mail, expire
        #User.set(user_id, name, icon_id, version)
        if mail:
            print mail
            #Mail.set(user_id, mail) 
        # session = UserSession.new(user_id)
        # self.set_cookie(
        #     'S', 
        #     session, 
        #     domain=HOST, 
        #     expires_days=(expire or None)
        # )

    def reload(self):
        next_link =  self.get_argument('next','/')
        self.finish(
            '<script>if(opener){opener.location.reload();window.close()}else if(parent!=window){parent.location.reload()}else{location="%s"}</script>'%next_link
        )


@route('/auth/user')
class _(View):
    def get(self):
        user = self.current_user
        if user:
            url = user.link
        else:
            url = '/'
        return self.redirect(url)

@route('/auth/name/set')
class _(View):
    def get(self):
        self.check_xsrf_cookie()
        if self.current_user_id:
            o = dumps([
                int(time()),
                self.current_user_id,
                self.get_argument("name").decode("utf-8","ignore").encode("utf-8","ignore"),
                HOST,
            ])
            return self.redirect(
                SSO_URL+'/auth/name/set?o=%s&sign=%s'%( quote(o) , sign(SSO_SECRET, o))
            )
        self.finish('<script>parent.location.href="/"</script>')


@route('/auth/logout')
class AuthLogout(View):
    def prepare(self):
        if self.get_argument('o', 0):
            super(AuthLogout, self).prepare()
        else:
            self.check_xsrf_cookie()

            o = dumps([
                int(time()),
                self.current_user_id,
                HOST
            ])
            self.redirect(
                SSO_URL+'/auth/logout?o=%s&sign=%s'%( quote(o) , sign(SSO_SECRET, o))
            )

    def reload(self):
        self.finish('<script>parent.location.href="/"</script>')

    def get(self):
        self.clear_cookie('S', domain=HOST)
        UserSession.rm_id(self.current_user_id)
        if self.get_argument('reload', 0):
            self.reload()




