#!/usr/bin/env python
# coding:utf-8

from google.appengine.ext import ndb

__metaclass__ = type

class Mail(ndb.Model):
    mail = ndb.BlobProperty('m')

    @classmethod
    def get(cls, id):
        id = int(id)
        if id:
            r = cls.get_by_id(id)
            if r:
                return r.mail
        return ''

    @classmethod
    def set(cls, id, value):
        id = int(id)
        if id:
            r = cls.get_by_id(id)
            if r is None:
                r = cls(id=id)
            r.mail = str(value)
            r.put()

