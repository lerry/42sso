#!/usr/bin/env python
#coding:utf-8

import _env
from zapp._plugin.app.model.sign import sign
from urllib import quote
from google.appengine.ext import ndb
from zapp._plugin._xae.gae.model.property.choice import ChoiceProperty
from misc.config import SSO_SECRET, HOST

class User(ndb.Model):

    name = ndb.BlobProperty('n')
    icon_id = ndb.IntegerProperty('I', default=0)
    version = ndb.IntegerProperty('v', default=1)
    level = ChoiceProperty([(10, 'FORBIDDEN'), (20, 'NORMAL'), (30, 'MANAGER')], name='L')

    @property
    def link(self):
        user_id = str(self.id)
        return 'https://42sso.sinaapp.com/auth/user/%s'%(
            user_id
        )

    @property
    def id(self):
        return self.key.id()

    @classmethod
    def set(cls, id, name, icon_id=0, version=1, key=None):
        id = int(id)

        if key is None:
            key = ndb.Key(User, id)

        entity = key.get()
        if not entity:
            entity = cls(
                level=cls.level.NORMAL,
                key=key
            )
        if type(name) is unicode:
            name = str(name)
        entity.name = name
        entity.icon_id = icon_id
        entity.version = version
        entity.put()
        return entity

    @classmethod
    def get(cls, id):
        id = int(id)
        if id:
            return ndb.Key(User, id).get()

    @classmethod
    def get_list(cls, id_li):
        return cls.query(cls.id.IN(id_li))


SYSTEM_USER = ndb.Key(User, 1)

if __name__ == '__main__':
    import sys
    if sys.getdefaultencoding() == 'ascii':
        reload(sys)
        sys.setdefaultencoding('utf-8')
