#!/usr/bin/env python
# coding:utf-8

from google.appengine.ext import ndb

__metaclass__ = type

class UserSession(ndb.Model):
    session = ndb.BlobProperty('s')

class KV:
    @staticmethod
    def get(id):
        id = int(id)
        if id:
            r = UserSession.get_by_id(id)
            if r:
                return r.session

    @staticmethod
    def set(id, value):
        id = int(id)
        if id:
            r = UserSession.get_by_id(id)
            if r is None:
                r = UserSession(id=id)
            r.session = value
            r.put()

    @staticmethod
    def delete(id):
        id = int(id)
        if id:
            ndb.Key(UserSession, id).delete()
