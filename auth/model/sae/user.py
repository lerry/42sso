#!/usr/bin/env python
# coding:utf-8

from _db import DB, Kv
__metaclass__ = type
USER_ICON_URL = Kv('UserIconUrl')
USER_ICON_ID = Kv('UserIconId')
USER_NAME = Kv('UserName')
USER_VERSION = Kv('UserVersion')

class User:
    def __init__(self, user_id):
        self.id = user_id

    @classmethod
    def new(cls, name, icon_url=''):
        self = cls._set(name)
        icon_id = 0
        if icon_url:
            icon_id = self.icon_new( icon_url)
        self.icon_id = icon_id
        return self.id

    @classmethod
    def get(cls, id):
        return cls(id)

    @classmethod
    def set(cls, id, name, icon_id, version):
        user = User(id)
        _version = user.version
        if _version != version:
            self = cls._set(name, version, id)
            self.icon_id = icon_id

    @classmethod
    def _set(cls, name, version=1, id=0):
        name = name.decode('utf-8', 'ignore').encode('utf-8', 'ignore')

        if id:
            user_id = int(id)
            DB.execute(
                'INSERT INTO User(id,name) VALUES (%s,%s) ON DUPLICATE KEY UPDATE name=%s',
                name,
                user_id,
                name
            )
        else:
            user_id = DB.insert_id('User', name=name)

        self = cls(user_id)
        self.version = version
        self.name = name
        return self


    def icon_new(self, icon_url):
        user_id = self.id
        icon_url = icon_url.strip()
        if not icon_url:
            return
        id = DB.insert_id('UserIcon', user_id=user_id)
        USER_ICON_URL.set(id, icon_url)
        return id

    @property
    def version(self):
        return USER_VERSION.get(self.id) or 0

    @version.setter
    def version(self, value):
        USER_VERSION.set(self.id, value)

    @property
    def name(self):
        return USER_NAME.get(self.id)

    @name.setter
    def name(self, value):
        USER_NAME.set(self.id, value)

    @property
    def icon_id(self):
        return USER_ICON_ID.get(self.id)

    @icon_id.setter
    def icon_id(self, value):
        return USER_ICON_ID.set(self.id, value)


