# coding:utf-8
import _env

from zapp._plugin.base.model.id_secret import IdSecret
try:
    from zapp._plugin._xae.sae.model._kv import Kv
except ImportError:
    #from zapp._plugin._xae.model.gae.user import
    from gae.user_session import KV
else:
    KV = Kv('S')


__metaclass__ = type

# KV 接口需要memcache兼容 , 这里用到 get , set , delete

class UserSession(IdSecret):
    KV = KV

if __name__ == '__main__':
    pass

