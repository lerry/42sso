#!/usr/bin/env python
# coding:utf-8


try:
    from zapp._plugin._xae.sae.model._kv import Kv
    Mail = Kv('Mail')
except ImportError:
    from gae.mail import Mail
