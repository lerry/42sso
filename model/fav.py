#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
from time import time
from model.db import Model
from model.data import Data

# fav 是独立的，与Data无关

class Fav(Model):
    @staticmethod
    def new(uid, obj_id):
        fav = Fav.get(obj_id=obj_id, uid=uid)
        if not fav:
            obj = Data.get(obj_id)
            if not obj:
                return
            fav = Fav(obj_id = obj_id, cid=obj.cid, uid=uid, time=int(time()))
            fav.save()
            return fav

    @staticmethod
    def rm(uid, obj_id):
        fav = Fav.get(obj_id=obj_id, uid=uid)
        if fav:
            fav.delete()
            return True

    def can_edit(self, uid):
        return self.uid == int(uid)

    @classmethod
    def count(cls, obj_id, **kwargs):
        return super(cls, Fav).count(obj_id=obj_id, **kwargs)

if __name__ == '__main__':
    pass

