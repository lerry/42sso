#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
import hashlib
from model.db import Model
from lib.jsdict import JsDict
from time import time

GENDER_DICT = JsDict({
    'm': 1,
    'f': 2
})

class USER_LEVEL:
    BAN    = 0
    NORMAL = 1
    VIP    = 4
    ADMIN  = 7

class User(Model):
    """ 用户对象，对应表 User，数据有 id mail name ico time gender """
    @staticmethod
    def new(mail, pw, name):
        if User.is_exists('mail', mail):
            return
        ret = User(mail=mail, pw=hashlib.md5(pw).digest(), name=name, time=int(time()), level=USER_LEVEL.NORMAL)
        ret.save()
        return ret

    @classmethod
    def passwd_verify(cls, mail, pw):
        _pw = hashlib.md5(pw).digest()
        return User.get(mail=mail, pw=_pw)

    @classmethod
    def get_users(cls):
        ret = cls.query('select * from User where level > %s', 0)
        data = ret.fetchall()
        result = []
        for i in data:
            result.append(cls._data_to_obj(i))
        return result

    @property
    def aico(self):
        ico = self.ico
        if ico:
            # 稍后做一些处理
            return 'http://che-avatar.b0.upaiyun.com/%s' % ico
        else:
            return 'http://che-avatar.b0.upaiyun.com/eb8bccca063dc043a7f5cc530e30e6b0'

if __name__ == '__main__':
    print User.get(2)
    pass
    #cur = User.new('fy0@qq.com', '123456','aaa')
    #print User.get(2).pw

