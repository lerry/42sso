# coding:utf-8

import _envi
from model.db import Model
from time import time

class CID_TYPE:
    Album = 1
    Photo = 2
    Fav   = 3
    Reply = 4

# 删除 草稿 私人 公开 推荐 精华
class DATA_STATUS:
    DELETE       = 0
    TEMP         = 2
    PRIVATE      = 4
    APPLY_FAILED = 8
    APPLY_PUBLIC = 16
    PUBLIC       = 32
    PUBLIC_STAR  = 64

class Data(Model):
    """ 数据 """
    @classmethod
    def new(cls, cid, uid, status):
        ret = cls(cid=cid, uid=uid, time=int(time()), status=status)
        ret.save()
        return ret

