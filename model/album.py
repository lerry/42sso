#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
from model.db import Model
from model.photo import album_photo_list, Photo
from model.data import Data, CID_TYPE, DATA_STATUS

class Album(Model):
    __data__ = None

    @classmethod
    def new(cls, name, txt, uid, status=DATA_STATUS.APPLY_PUBLIC):
        d = Data.new(CID_TYPE.Album, uid, status)
        album = Album(
            id=d.id,
            title=name,
            txt=txt,
        )
        album.save()
        return album

    @staticmethod
    def user_album_latest(limit, offset, uid, order_by='id DESC'):
        return [Album.get(x.id) for x in Data.where(limit=limit, offset=offset, order_by=order_by, uid=uid, cid=CID_TYPE.Album)]

    @staticmethod
    def count(level=DATA_STATUS.PUBLIC):
        ret = Album.query('select count(*) from Data where cid=%s and status=%s', (CID_TYPE.Album, level))
        data = ret.fetchone()
        return data[0] if data else 0

    @staticmethod
    def latest(limit, offset, level=DATA_STATUS.PUBLIC, order_by='id DESC'):
        ret = Data.query('select * from Data where cid=%s and status=%s'+' order by %s limit %s offset %s'%(order_by, limit, offset), (CID_TYPE.Album, level))
        data = ret.fetchall()
        lst = []
        for i in data:
           lst.append(Data._data_to_obj(i))

        new_lst = []
        for i in lst:
            newitem = Album.get(i.id)
            newitem.__data__ = i
            new_lst.append(newitem)
        return new_lst

    @property
    def uid(self):
        if not self.__data__:
            self.__data__ = Data.get(self.id)
        return int(self.__data__.uid)

    @property
    def status(self):
        if not self.__data__:
            self.__data__ = Data.get(self.id)
        return int(self.__data__.status)

    @property
    def photos(self):
        return album_photo_list(self.id, limit=100)

    def can_edit(self, uid):
        return self.uid == int(uid)

    @property
    def cover_link(self):
        p = Photo.get(album_id=self.id)
        return p.link if p else ''

if __name__ == '__main__':
    pass

