#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
from time import time
from model.db import Model
from model.user import User, USER_LEVEL
from model.data import Data, CID_TYPE, DATA_STATUS

class Reply(Model):
    __data__ = None

    @staticmethod
    def new(uid, obj_id, txt, status=DATA_STATUS.PUBLIC):
        if not Data.get(obj_id): return
        data = Data.new(CID_TYPE.Reply, uid, status)
        reply = Reply(id=data.id, obj_id = obj_id, txt=txt)
        reply.save()
        reply.__data__ = data
        return reply

    @staticmethod
    def rm(uid, id):
        data = Data.get(id=id)
        if data.status != DATA_STATUS.DELETE:
            data.status = DATA_STATUS.DELETE
            return True

    def can_edit(self, uid):
        return self.uid == int(uid)

    @property
    def time(self):
        if not self.__data__:
            self.__data__ = Data.get(self.id)
        return int(self.__data__.time)

    @property
    def uid(self):
        if not self.__data__:
            self.__data__ = Data.get(self.id)
        return int(self.__data__.uid)

    @property
    def status(self):
        if not self.__data__:
            self.__data__ = Data.get(self.id)
        return int(self.__data__.status)

    @classmethod
    def count(cls, obj_id, **kwargs):
        return super(cls, Reply).count(obj_id=obj_id, **kwargs)

if __name__ == '__main__':
    pass

