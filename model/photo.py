#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
from time import time
from binascii import a2b_hex, b2a_hex
from model.db import Model
from model.data import Data, CID_TYPE, DATA_STATUS
from config import UPYUN

URL_BASE = 'http://%s/' % UPYUN['cheshang'][1] + '%s'

class Photo(Model):
    @property 
    def url(self):
        return b2a_hex(self.md5)

    @property
    def photos(self):
        return album_photo_list(self.id, limit=100)

    @property
    def link(self):
        return URL_BASE % self.url

    @classmethod
    def new(cls, url, title, album_id, size, uid, status=DATA_STATUS.PUBLIC):
        d = Data.new(CID_TYPE.Photo, uid, status)
        w, h = map(int, size.split(','))
        photo = Photo(
            id       = d.id,
            md5      = a2b_hex(url),
            title    = title,
            album_id = album_id,
            width    = w,
            height   = h,
        )
        photo.save()
        return photo

def album_photo_list(album_id, limit=1, offset=0):
    return Photo.where(
        album_id=album_id, 
        limit=limit, 
        offset=offset
    )

        
if __name__ == '__main__':
    pass
    #print album_photo_list(1000000, limit=100)
    #print Photo.get(album_id=1000002)

