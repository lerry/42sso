#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
import re
import json
import tornado.web
from os.path import join, exists
#from model.user import User, USER_LEVEL
from lib.utils import ERR
from lib.render import render_jinja
from lib.template_extend import FUNCTIONS
#from model.session import session_new, id_by_session, session_rm

TEMPLATE_PATH = join(_envi.PREFIX, 'html')

render = render_jinja(
    TEMPLATE_PATH,
    encoding='utf-8',
)

class Route(object):
    def __init__(self):
        self.urls = []

    def __call__(self, url):
        def _(cls):
            self.urls.append((url, cls))
            return cls
        return _

route = Route()

def logout(self):
    s = self.get_secure_cookie('S')
    self.set_secure_cookie('S', '')
    session_rm(s)

def login(self, uid):
    self.set_secure_cookie('S', session_new(uid), domain=self.request.host, expires_days=999)

class View(tornado.web.RequestHandler):
    # @property
    # def current_user_id(self):
    #     s = self.get_secure_cookie('S')
    #     return id_by_session(s)

    # @property
    # def current_user(self):
    #     uid = self.current_user_id
    #     return User.get(uid) if uid else None

    # user = current_user
    # uid = current_user_id

    def render(self, filename=None, **kwargs):
        # kwargs['current_user_id'] = self.current_user_id
        # kwargs['current_user'] = self.current_user
        _xsrf = self.xsrf_form_html()
        kwargs.update(FUNCTIONS)
        kwargs['_xsrf'] = _xsrf
        if not filename:
            filename = '%s/%s.html' % (
                self.__module__[5:].replace('.', '/').lower(),
                self.__class__.__name__.lower()
            )
            if not exists(join(TEMPLATE_PATH, filename)):
                filename = '%s/%s.html' % (
                    self.__module__[5:].replace('.', '/').lower(),
                    re.sub('([A-Z])', r'-\1',self.__class__.__name__)[1:].lower()
                )
        self.finish(render._render(filename, **kwargs))

    # login = login
    # logout = logout

# class LoginView(View):
#     def prepare(self):
#         super(LoginView, self).prepare()
#         if not self.current_user_id:
#             self.render('error.html', err=ERR.NO_LOGIN)
#             return

# class JsonView(View):
#     def finish(self, arg=None):
#         self.set_header('Content-Type', 'application/json; charset=UTF-8')
#         if arg and not isinstance(arg, basestring):
#             arg = json.dumps(arg)
#         super(JsonView, self).finish(arg)

# class JsonLoginView(JsonView):
#     def prepare(self):
#         super(JsonLoginView, self).prepare()
#         if self._finished:
#             return
#         if not self.current_user_id:
#             self.finish({'err':ERR.NO_LOGIN})

# class JsonUserView(JsonLoginView):
#     def prepare(self):
#         super(JsonUserView, self).prepare()
#         if self._finished:
#             return
#         if self.user.level == USER_LEVEL.BAN:
#             self.finish({'err':ERR.PERMISSION_DENIED})

# class NoLoginView(View):
#     def prepare(self):
#         if self.current_user_id:
#             self.redirect('/me')

# class AdminView(LoginView):
#     def prepare(self):
#         super(AdminView, self).prepare()
#         u = self.current_user
#         if u and u.level != USER_LEVEL.ADMIN:
#             self.render('error.html', err=ERR.PERMISSION_DENIED)
#             return

# class JsonAdminView(JsonLoginView):
#     def prepare(self):
#         super(JsonAdminView, self).prepare()
#         u = self.current_user
#         if u and u.level != USER_LEVEL.ADMIN:
#             self.finish({'err':ERR.PERMISSION_DENIED})
#             return

