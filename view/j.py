#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
import web
import config
from time import time
from lib.utils import RE_EMAIL
from view._base import route, JsonView, JsonLoginView, JsonAdminView, JsonUserView
from model.fav import Fav
from model.user import User
from model.album import Album
from model.reply import Reply
from model.user import USER_LEVEL
from lib.utils import ERR
from model.data import Data, CID_TYPE, DATA_STATUS

@route('/j/boss/album_pass/(\d+)')
class AlbumPass(JsonAdminView):
    def post(self, id):
        data = Data.get(id)
        if not data:
            self.finish({'err':ERR.OPERATION_FAILED})
            return
        status = data.status
        if status == DATA_STATUS.APPLY_PUBLIC:
            data.status = DATA_STATUS.PUBLIC
            data.save()
            self.finish({'err':0})
        else:
            self.finish({'err':ERR.OPERATION_FAILED})
    get = post

@route('/j/boss/album_reject/(\d+)')
class AlbumReject(JsonAdminView):
    def post(self, id):
        data = Data.get(id)
        if not data:
            self.finish({'err':ERR.OPERATION_FAILED})
            return
        status = data.status
        if status == DATA_STATUS.APPLY_PUBLIC:
            data.status = DATA_STATUS.APPLY_FAILED
            data.save()
            self.finish({'err':0})
        else:
            self.finish({'err':ERR.OPERATION_FAILED})
    get = post

@route('/j/boss/album_delete/(\d+)')
class AlbumDelete(JsonAdminView):
    def post(self, id):
        data = Data.get(id)
        if not data:
            self.finish({'err':ERR.OPERATION_FAILED})
            return
        data.status = DATA_STATUS.DELETE
        data.save()
        self.finish({'err':0})
    get = post

@route('/j/boss/album_star/(\d+)')
class AlbumStar(JsonAdminView):
    def post(self, id):
        data = Data.get(id)
        if not data:
            self.finish({'err':ERR.OPERATION_FAILED})
            return
        status = data.status
        if status == DATA_STATUS.PUBLIC:
            data.status = DATA_STATUS.PUBLIC_STAR
            data.save()
            self.finish({'err':0})
        else:
            self.finish({'err':ERR.OPERATION_FAILED})
    get = post

@route('/j/boss/album_unstar/(\d+)')
class AlbumUnStar(JsonAdminView):
    def post(self, id):
        data = Data.get(id)
        if not data:
            self.finish({'err':ERR.OPERATION_FAILED})
            return
        status = data.status
        if status == DATA_STATUS.PUBLIC_STAR:
            data.status = DATA_STATUS.PUBLIC
            data.save()
            self.finish({'err':0})
        else:
            self.finish({'err':ERR.OPERATION_FAILED})
    get = post

@route('/j/boss/user/ban/(\d+)')
class UserBan(JsonAdminView):
    def post(self, uid):
        u = User.get(id=uid)
        if u.level >= self.user.level:
            self.finish({'err':ERR.PERMISSION_DENIED})
            return
        u.level = USER_LEVEL.BAN
        u.save()

@route('/j/boss/user/unban/(\d+)')
class UserBan(JsonAdminView):
    def post(self, uid):
        u = User.get(id=uid)
        if u.level == USER_LEVEL.BAN:
            u.level = USER_LEVEL.NORMAL
            u.save()
        else:
            self.finish({'err':ERR.OPERATION_FAILED})

@route('/j/email/verify')
class email_exists(JsonView):
    def post(self):
        email = self.get_argument('email')
        if not RE_EMAIL.match(email):
            self.finish('邮箱格式不正确')
        elif User.is_exists('mail', email):
            self.finish('邮箱已被注册')
        else:
            self.finish('ok')
    get = post


@route('/j/catagory/new')
class cata_new(JsonLoginView):
    def post(self):
        uid = self.current_user_id
        name = self.get_argument('name')
        if name:
            Album.new(name,'', uid)
        return self.render(1)

@route('/j/album/(\d+)/upload')
class Upload(JsonLoginView):
    def post(self, album_id=0):
        album = Album.get(album_id)
        if not album or not album.can_edit(self.uid):
            return self.render(0)
        uid = self.current_user_id
        urls,names,sizes = self.get_arguments('url'), self.get_arguments('name'), self.get_arguments('size')
        for url, name, size  in zip(urls, names, sizes):
            if url and size:
                Photo.new(url, name, album_id, size, uid)
        return self.render(1)

@route('/j/fav/(\d+)')
class FavNew(JsonLoginView):
    def post(self, obj_id):
        if Fav.new(self.uid, obj_id):
            self.finish({'err':0})
        else:
            self.finish({'err':ERR.OPERATION_FAILED})
    get = post

@route('/j/unfav/(\d+)')
class FavRm(JsonLoginView):
    def post(self, obj_id):
        if Fav.rm(self.uid, obj_id):
            self.finish({'err':0})
        else:
            self.finish({'err':ERR.OPERATION_FAILED})
    get = post

@route('/j/reply/(\d+)')
class ReplyGet(JsonView):
    def get(self, album_id):
        ret = []
        #lst = Reply.where(10, 0, 'id', '=', obj_id=album_id, status=DATA_STATUS.PUBLIC)
        lst = Reply.where(obj_id=album_id)
        for i in lst:
            ret.append({'id':i.id, 'uid':i.uid, 'time':i.time, 'status':i.status, 'txt':i.txt})
        self.finish(ret)

    post = get

@route('/j/reply/new/(\d+)')
class ReplyPost(JsonUserView):
    def post(self, obj_id):
        txt = self.get_argument('txt', '')
        status = int(self.get_argument('stat', DATA_STATUS.PUBLIC))

        data = Data.get(order_by='id desc', cid=CID_TYPE.Reply, uid=self.uid)
        if data and ((int(time()) - data.time) <= config.REPLY_INTERVAL):
            self.finish({'err':ERR.POST_TOO_QUICK})
            return
        if txt:
            Reply.new(self.uid, obj_id, txt, status)
        else:
            self.finish({'err':ERR.OPERATION_FAILED})

    get = post

