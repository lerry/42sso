#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
from view._base import route, View

@route('/')
class Index(View):
    def get(self):
        self.render()
