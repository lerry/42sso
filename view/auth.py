#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
import web
from config import render, OAUTH2_CONFIG
from view._base import route, View, LoginView, NoLoginView
from lib.weibo import APIClient as weibo_client
from lib.renren import APIClient as renren_client
from lib.jsdict import JsDict
from model.oauth2 import oauth2_new, uid_by_oauth, OAUTH_TYPE
from model.user import User

@route('/signup')
class Signup(NoLoginView):
    def get(self):
        self.render()

    def post(self):
        geta = lambda x : self.get_argument(x)
        email, passwd, name = geta('email'), geta('passwd'), geta('name')
        if email and passwd:
            u = User.new(email, passwd, name)
            if u:
                self.redirect('/signin')
                return
        self.render()

@route('/signin')
class Signin(NoLoginView):
    def get(self):
        self.render()

    def post(self):
        u = self.get_argument('u')
        p = self.get_argument('p')
        ret = User.passwd_verify(u, p)
        if ret:
            self.login(ret.id)
            self.redirect('/me')
        else:
            # 密码不对
            self.render()

@route('/signout')
class Signout(LoginView):
    def get(self):
        self.logout()
        return self.redirect('/')

@route('/callback/(\w+)')
class Callback(View):
    def get(self, oauth_type):
        code = self.argument('code')
        if oauth_type in OAUTH_TYPE.keys():
            CALLBACK_URL = 'http://ldev.cn/callback/%s' % oauth_type
            key, secret = OAUTH2_CONFIG[oauth_type]
            APIClient = globals()['%s_client' % oauth_type]
            client = APIClient(app_key=key, app_secret=secret, redirect_uri=CALLBACK_URL)
            r = client.request_access_token(code)
            if oauth_type == 'weibo':
                r = JsDict(r)
            access_token = r.access_token # 新浪返回的token
            expires_in = r.expires_in # token过期的UNIX时间：  
            oauth_uid = r.uid if oauth_type == 'weibo' else r.user.id
            uid = oauth2_new(oauth_type=oauth_type, oauth_id=oauth_uid,\
                token=r.access_token, expires_in=r.expires_in, refresh_token=r.refresh_token)
            client.set_access_token(access_token, expires_in)
            if oauth_type == 'weibo':
                info = client.users.show.get(uid=r.uid)
                profile_new(uid, 
                    name = info['screen_name'],
                    motto = info['description'],
                    avatar = info['avatar_large'],
                    gender = GENDER_DICT.get(info['gender'], 0))
            else:
                info = r#client.api_call('users.getInfo')
                profile_new(uid, 
                    name = info['name'],
                    avatar = r.user.avatar[-1]['url'])
            self.login(uid)
            self.redirect('/me')

@route('/oauth/(\w+)')
class Weibo(View):
    def get(self, oauth_type):
        CALLBACK_URL = 'http://ldev.cn/callback/%s' % oauth_type
        key, secret = OAUTH2_CONFIG[oauth_type]
        APIClient = globals()['%s_client' % oauth_type]
        client = APIClient(app_key=key, app_secret=secret, redirect_uri=CALLBACK_URL)
        url = client.get_authorize_url()
        self.redirect(url)
