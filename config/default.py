#!/usr/bin/python
# -*- coding: utf-8 -*-
import _envi
import web
import platform


DEBUG = True

HOST = "lerry.me"
PORT = 80

MYSQL_HOST = '127.0.0.1'
MYSQL_PORT = 3306
MYSQL_USER = 'work'
MYSQL_PASSWD = 'pw'
MYSQL_DB = 'cheshang_dev'

MEMCACHED_ADDR = '127.0.0.1:11211'

SSO_SECRET = 'aaaaaaaaaaaaaa'

# 30 secs
REPLY_INTERVAL = 30