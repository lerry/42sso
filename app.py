#!/usr/bin/python
# coding: utf-8
import _envi
import tornado.ioloop
import tornado.web
import tornado.options
import tornado.autoreload
import config
import view._site
from os.path import join
from view._base import route, View

import auth.view._url

from auth.view._route import route as _route


application = tornado.web.Application(
    route.urls+_route.urls,
    debug=config.DEBUG,
    static_path=join(_envi.PREFIX, 'static'),
    cookie_secret="6aOO5ZC55LiN5pWj6ZW/5oGo77yM6Iqx5p+T5LiN6YCP5Lmh5oSB44CC",
)

if __name__ == "__main__":
    def fn():
            print "Code changed, reloading..."
    tornado.options.parse_command_line()
    application.listen(config.PORT)            
    tornado.autoreload.add_reload_hook(fn)
    tornado.autoreload.start()
    tornado.ioloop.IOLoop.instance().start()        
