# 42SSO #

### 单点登录及统一的帐号系统 ###

写过很多很多网站  
每次写网站  
都要重新折腾一遍   

    注册 
    登录 / 微博&豆瓣&QQ登录(还有绑定邮箱)    
    发送激活邮件 
    找回密码   
    更改邮箱   
    上传头像   
    剪裁头像   
    绑定手机   
    填写地址   
    用户设置自己的个性网址    
    ... 

我需要一个方案，一个一劳永逸的解析方案   
那就是   
*网站无关* 的通行证    
***

各种基于
Oauth2的微博登录
是不是已经解决了这个问题？  
我的回答是 :  

    我要的不是一个协议
        而是一个解决方案
    包括了 ：
    页面样式 , 交互 , 后端存储 ...

***
#### 登录流程 ####
用户点击登录按钮    
-> 动态加载登录的JS (配合require.js)      
-> 通过JSONP 调用 42sso.sinaapp.com  
-> 42sso 验证  
-> 成功   
-> 重定向到 42btc 保存登录信息   
-> 设置 42btc.com 的 cookie  
-> 出错  
-> 显示错误信息  
***
#### 如何使用 ####

##### 后端部分 #####
你需要用一个View接收回调信息  
包括（time, user_id, version, name, icon_id, mail, expire） 
为了保证安全，每次通信都会生成一个sign用于校验  
见项目的 *auth* 文件夹  

代码以tornado框架为例  
+ 将auth文件夹放入你的项目文件夹  
+ 在你的入口文件，将auth的路由引入

    import auth.view._url
    from auth.view._route import route as _route
    application = tornado.web.Application(
        route.urls+_route.urls,#route是原有的
        debug=config.DEBUG,
        static_path=join(_envi.PREFIX, 'static'),

这样后端部分就好了
##### 前端部分 #####
请参考 html/index/index.html文件     
引用必须的css和js文件

    <script src="/static/js/base.js"></script>
    <script src="/static/js/login.js"></script>
    <link rel="stylesheet" href="/static/css/login.css">
    <link rel="stylesheet" href="/static/css/css3-facebook-buttons/fb-buttons.css">
    <link rel="stylesheet" href="/static/js/lib/fancybox/jquery.fancybox.css" >
    <link rel="stylesheet" href="/static/css/fancybox.css">

在合适的地方放上登录链接

    <a href="javascript:$.SSO.login();void(0)">登录</a>
    <a href="javascript:$.SSO.login();void(0)">注册</a>

这时运行你的程序应该能看到登陆页面了  
它会提示你
    403: please generate a secret key first , visit this link https://42sso.sinaapp.com/app/new?host=127.0.0.1

请联系我们生成秘钥，并写在配置文件里就可以了    